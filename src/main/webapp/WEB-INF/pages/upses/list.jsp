<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>list of lockers</title>
</head>
<body>
<div>
    <jsp:directive.page contentType="text/html;charset=UTF-8"/>
    <h1>Список упсов</h1>
    <c:if test="${not empty upses}">
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Model</th>
                <th>Locker</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${upses}" var="locker">
                <tr>
                    <td>${locker.id}</td>
                    <td>${locker.model}</td>
                    <td>${locker.locker}</td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>

<div>
    <a href="/serverInventory/newUps">Добавить UPS</a>
</div>


</body>
</html>


private Long id;
private String model;
private Locker locker;