package ru.maximoff.domain;

import javax.persistence.*;

@Entity
@Table(name = "DISC")
public class Disc {

    private Long id;
    private String serialNumber;
    private String model;
    private Long capacity;
    private RAID raid;

    public Disc() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SERIAL_NUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Column(name = "MODEL")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Column(name = "CAPACITY")
    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    @ManyToOne
    @JoinColumn(name = "RAID_ID")
    public RAID getRaid() {
        return raid;
    }

    public void setRaid(RAID raid) {
        this.raid = raid;
    }
}
