package ru.maximoff.domain;

import javax.persistence.*;

@Entity
@Table(name = "SERVER")
public class Server {

    private Long id;
    private String model;
    private Locker locker;

    public Server() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MODEL")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @ManyToOne
    @JoinColumn(name = "LOCKER_ID")
    public Locker getLocker() {
        return locker;
    }

    public void setLocker(Locker locker) {
        this.locker = locker;
    }
}
