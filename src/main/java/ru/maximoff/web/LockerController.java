package ru.maximoff.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maximoff.domain.Locker;
import ru.maximoff.service.LockerService;
import ru.maximoff.service.RaidService;
import ru.maximoff.service.ServerService;
import ru.maximoff.service.UpsService;

import java.util.List;

@Controller
public class LockerController {

    @Autowired
    private LockerService lockerService;

    @Autowired
    private UpsService upsService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private RaidService raidService;

    @RequestMapping("/lockers")
    public String list(ModelMap model) {
        List<Locker> lockers = lockerService.getAll();
        model.addAttribute("lockers", lockers);
        return "lockers/list";
    }

    @RequestMapping("/newLocker")
    public String newLocker(ModelMap model) {
        model.addAttribute("locker", new Locker());
        return "lockers/newLocker";
    }

    @RequestMapping(value = "/newLocker", method = RequestMethod.POST)
    public String addLocker(ModelMap model, @ModelAttribute("locker") Locker locker, BindingResult result) {
        if (result.hasErrors()) {
            return newLocker(model);
        }
        lockerService.add(locker);
        return "redirect:/lockers";
    }

    @RequestMapping("/removelocker/{id}")
    public String removeLocker(@ModelAttribute Long id)
    {
        lockerService.deleteById(id);
        return "redirect:/lockers";
    }


    @RequestMapping("locker/{id}")
    public String lockerDetails(ModelMap model, @PathVariable Long id) {
        model.addAttribute("locker", lockerService.getById(id));
        model.addAttribute("ups", upsService.loadByLockerId(id));
        model.addAttribute("servers", serverService.loadServersByLockerId(id));
        model.addAttribute("raid", raidService.loadRaidByLockerId(id));
        return "lockers/lockerDetail";
    }
}