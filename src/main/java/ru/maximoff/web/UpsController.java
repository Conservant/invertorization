package ru.maximoff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.maximoff.domain.UPS;
import ru.maximoff.service.UpsService;

import java.util.List;

@Controller
public class UpsController {

    @Autowired
    private UpsService upsService;

    @RequestMapping("/upses")
    public String list(ModelMap model) {
        List<UPS> upses = upsService.getAll();
        model.addAttribute("upses", upses);
        return "upses/list";
    }

    @RequestMapping("/newUps")
    public String newUPS(ModelMap model) {
        model.addAttribute("ups", new UPS());
        return "upses/newUps";
    }

    @RequestMapping(value = "/newUps", method = RequestMethod.POST)
    public String addLocker(ModelMap model, @ModelAttribute("ups") UPS ups, BindingResult result) {
        if (result.hasErrors()) {
            return newUPS(model);
        }
        upsService.add(ups);
        return "redirect:/upses";
    }
}
