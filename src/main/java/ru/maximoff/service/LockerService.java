package ru.maximoff.service;

import ru.maximoff.domain.Locker;

public interface LockerService extends AbstractService<Locker> {
}
