package ru.maximoff.service.impl;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maximoff.domain.Server;
import ru.maximoff.service.ServerService;

import java.util.List;

@Service(value = "serverService")
@Repository
@Transactional
public class ServerServiceImpl implements ServerService {

    @Override
    public void add(Server server) {

    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void update(Server server) {

    }

    @Override
    public Server getById(Long id) {
        return null;
    }

    @Override
    public List<Server> getAll() {
        return null;
    }

    @Override
    public List<Server> loadServersByLockerId(Long id) {
        return null;
    }
}
