package ru.maximoff.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maximoff.dao.RaidRepository;
import ru.maximoff.domain.RAID;
import ru.maximoff.service.RaidService;

import java.util.List;

@Service(value = "raidService")
@Repository
@Transactional
public class RaidServiceImpl implements RaidService {

    @Autowired
    private RaidRepository raidRepository;

    @Override
    public void add(RAID raid) {
        raidRepository.saveAndFlush(raid);
    }

    @Override
    public void deleteById(Long id) {
        raidRepository.delete(id);
    }

    @Override
    public void update(RAID raid) {

    }

    @Override
    public RAID getById(Long id) {
        return raidRepository.findOne(id);
    }

    @Override
    public List<RAID> getAll() {
        return raidRepository.findAll();
    }

    @Override
    public List<RAID> loadRaidByLockerId(Long id) {
        return null;
    }
}
