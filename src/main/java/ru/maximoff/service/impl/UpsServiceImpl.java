package ru.maximoff.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maximoff.dao.LockerRepository;
import ru.maximoff.dao.UpsRepository;
import ru.maximoff.domain.Locker;
import ru.maximoff.domain.UPS;
import ru.maximoff.service.UpsService;

import java.util.List;

@Service(value = "upsService") //
@Repository
@Transactional
public class UpsServiceImpl implements UpsService {

    @Autowired
    private UpsRepository upsRepository;

    @Autowired
    private LockerRepository lockerRepository;

    @Override
    public void add(UPS ups) {
        upsRepository.saveAndFlush(ups);
    }

    @Override
    public void deleteById(Long id) {
        upsRepository.delete(id);
    }

    @Override
    public void update(UPS ups) {

    }

    @Override
    public UPS getById(Long id) {
        return upsRepository.findOne(id);
    }

    @Override
    public List<UPS> getAll() {
        return upsRepository.findAll();
    }

    @Override
    public UPS loadByLockerId(Long id) {
        Locker one = lockerRepository.findOne(id);
        if (one != null) {
            return one.getUPS();
        }
        return null;
    }
}
