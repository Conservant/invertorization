package ru.maximoff.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maximoff.dao.LockerRepository;
import ru.maximoff.domain.Locker;
import ru.maximoff.service.LockerService;

import java.util.List;

@Service(value = "lockerService")
@Repository
@Transactional
public class LockerServiceImpl implements LockerService {

    @Autowired
    private LockerRepository lockerRepository;

    @Override
    public void add(Locker locker) {
        lockerRepository.saveAndFlush(locker);
    }

    @Override
    public void deleteById(Long id) {
        lockerRepository.delete(id);

    }

    @Override
    public void update(Locker locker) {

    }

    @Override
    public Locker getById(Long id) {
        return lockerRepository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Locker> getAll() {
        return lockerRepository.findAll();
    }
}
