package ru.maximoff.service.impl;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.maximoff.domain.Disc;
import ru.maximoff.service.DiscService;

import java.util.List;

@Service(value = "discService")
@Repository
@Transactional
public class DiscServiceImpl implements DiscService {

    @Override
    public void add(Disc disc) {

    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void update(Disc disc) {

    }

    @Override
    public Disc getById(Long id) {
        return null;
    }

    @Override
    public List<Disc> getAll() {
        return null;
    }
}
