package ru.maximoff.service;

import ru.maximoff.domain.RAID;

import java.util.List;

public interface RaidService extends AbstractService<RAID> {
    List<RAID> loadRaidByLockerId(Long id);
}
