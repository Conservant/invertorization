package ru.maximoff.service;

import java.util.List;

public interface AbstractService<T> {
    void add(T t);
    void deleteById(Long id);
    void update(T t);
    T getById(Long id);
    List<T> getAll();
}
