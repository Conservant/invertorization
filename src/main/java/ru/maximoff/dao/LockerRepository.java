package ru.maximoff.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maximoff.domain.Locker;

public interface LockerRepository extends JpaRepository<Locker, Long> {
}
