package ru.maximoff.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maximoff.domain.RAID;

public interface RaidRepository extends JpaRepository<RAID, Long> {
}
