package ru.maximoff.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maximoff.domain.Server;

public interface ServerRepository extends JpaRepository<Server, Long> {
}
