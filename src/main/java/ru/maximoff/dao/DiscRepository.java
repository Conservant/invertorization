package ru.maximoff.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maximoff.domain.Disc;

public interface DiscRepository extends JpaRepository<Disc, Long> {
}
