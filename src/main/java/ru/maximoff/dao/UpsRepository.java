package ru.maximoff.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maximoff.domain.UPS;

public interface UpsRepository extends JpaRepository<UPS, Long> {
}
